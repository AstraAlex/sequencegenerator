package ru.anovikov.interview.sequence.core;

import ru.anovikov.interview.sequence.persistence.DBConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDatabaseSequence {

    protected final String dbName;
    protected final Integer startFrom;
    protected final Integer endWith;
    protected final boolean isCycled;

    protected ThreadLocal<Integer> currentValue = new ThreadLocal<>();
    protected boolean isLoaded = false;

    protected AbstractDatabaseSequence(String dbName, Integer startFrom, Integer endWith, boolean isCycled) {
        if (dbName == null || dbName.isEmpty()) throw new IllegalArgumentException("Укажите название последовательности");
        if (startFrom == null) startFrom = 0;

        this.dbName = dbName;
        this.startFrom = startFrom;
        this.endWith = endWith;
        this.isCycled = isCycled;
    }

    protected boolean load() {
        if (isLoaded) return true;

        StringBuilder createSQL = new StringBuilder("CREATE SEQUENCE IF NOT EXISTS ").append(dbName);

        if (startFrom != null && startFrom != 0) {
            createSQL.append(" MINVALUE ").append(startFrom);
        }

        if (endWith != null) {
            createSQL.append(" MAXVALUE ").append(endWith);
        }

        if (isCycled) {
            createSQL.append(" CYCLE");
        }

        try {
            Statement st = DBConnector.INSTANCE.getConnection().createStatement();
            st.execute(createSQL.toString());
            st.close();

            isLoaded = true;
        } catch (SQLException e) {
            System.err.println("Ошибка при выполнении запроса создания: " + e);
            isLoaded = false;
        }

        return isLoaded;
    }

    protected Integer current() {
        if (currentValue.get() == null) {
            next();
        }

        return currentValue.get();
    }

    protected Integer next() {
        String nextSql = String.format("SELECT nextval('%s')", dbName);

        try {
            Statement st = DBConnector.INSTANCE.getConnection().createStatement();
            ResultSet rs = st.executeQuery(nextSql);

            while ( rs.next() ) {
                currentValue.set(rs.getInt("nextval"));
            }

            rs.close();
            st.close();

        } catch (SQLException e) {
            System.err.println("Ошибка при выполнении запроса следующего номера: " + e);
            System.exit(1);
        }

        return currentValue.get();
    }

    public String getDbName() {
        return dbName;
    }

    public Integer getStartFrom() {
        return startFrom;
    }

    public Integer getEndWith() {
        return endWith;
    }

    public boolean isCycled() {
        return isCycled;
    }
}
