package ru.anovikov.interview.sequence.core;

public interface Sequence {

    /**
     * Состояние связи последовательности с БД.
     *
     * Если последовательности не существует в БД, она создается.
     * Если последовательность уже есть, подключает к ней java объект
     *
     * @return true, если объект связан и загружен, false - ошибки при загрузке
     */
    boolean load();

    /**
     * Загружает из БД следующий свободный для использования номер последовательности,
     * делает его текущим и возвращает.
     *
     * @return следующий свободный номер последовательности.
     */
    Object nextValue();

    /**
     * @return Текущий свободный для использования номер последовательности.
     */
    Object currentNum();

}
