package ru.anovikov.interview.sequence.core.impl;

import ru.anovikov.interview.sequence.core.AbstractDatabaseSequence;
import ru.anovikov.interview.sequence.core.Sequence;

public class NumberSequence extends AbstractDatabaseSequence implements Sequence {

    public NumberSequence(String dbName, Integer startFrom, Integer endWith, boolean isCycled) {
        super(dbName, startFrom, endWith, isCycled);
    }

    @Override
    public boolean load() {
        return super.load();
    }

    @Override
    public Integer currentNum() {
        return super.current();
    }

    @Override
    public Integer nextValue() {
        return super.next();
    }
}
