package ru.anovikov.interview.sequence.core.impl;

import ru.anovikov.interview.sequence.core.AbstractDatabaseSequence;
import ru.anovikov.interview.sequence.core.Sequence;

/**
 * Последовательность строк из шаблона
 * Символы ~ (и все, что внутри них) будут заменены на цифры последовательности
 */
public class StringSequence extends AbstractDatabaseSequence implements Sequence {

    private final String SPECIAL_SYMBOLS = "~";
    private String template;
    private int startIndex;
    private int endIndex;
    private int countToReplace;

    public StringSequence(String dbName, Integer startFrom, Integer endWith, boolean isCycled, String template) {
        super(dbName, startFrom, endWith, isCycled);
        if (template == null || !template.contains("~")) throw new IllegalArgumentException("Неверный шаблон");

        this.template = template;
        this.startIndex = template.indexOf(SPECIAL_SYMBOLS);
        this.endIndex = template.lastIndexOf(SPECIAL_SYMBOLS) + 1; // include itself
        this.countToReplace = endIndex - startIndex;
    }


    @Override
    public boolean load() {
        return super.load();
    }

    @Override
    public String nextValue() {
        return wrap(super.current());
    }

    @Override
    public String currentNum() {
        return wrap(super.next());
    }

    private String wrap(Integer num) {
        StringBuilder intSequence = new StringBuilder(num.toString());

        while (intSequence.length() < countToReplace) {
            intSequence.insert(0, "0");
        }

        return template.substring(0, startIndex) + intSequence + template.substring(endIndex);
    }
}
