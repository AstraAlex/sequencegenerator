package ru.anovikov.interview.sequence.persistence;

import java.sql.*;

public class DBConnector {

    public static final DBConnector INSTANCE = new DBConnector();

    private final String JDBC_URL = "jdbc:postgresql://localhost:5432/sequence_generator";
    private final String USER = "sequence_generator";
    private final String PASS = "sequence_generator";
    private Connection connection;

    private DBConnector() {
        try {
            Driver pgDriver = new org.postgresql.Driver();
            DriverManager.registerDriver(pgDriver);
            connection = DriverManager.getConnection(JDBC_URL, USER, PASS);
        } catch (SQLException e) {
            System.out.println("Проблемы с подключением к БД: " + e);
            System.exit(1);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        connection.close();
    }
}
