package ru.anovikov.interview.sequence;

import org.junit.Ignore;
import org.junit.Test;
import ru.anovikov.interview.sequence.persistence.DBConnector;

import java.sql.*;

public class DatabaseConnectionTest {

    @Test
    @Ignore
    public void connectToDB() throws Exception {
        try {
            Statement st = DBConnector.INSTANCE.getConnection().createStatement();
            ResultSet rs = st.executeQuery("CREATE SEQUENCE IF NOT EXISTS test_connection_seq");
            while ( rs.next() )
            {
                System.out.println(rs.getString("nextval"));
            }
            rs.close();
            st.close();
        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
    }
}
