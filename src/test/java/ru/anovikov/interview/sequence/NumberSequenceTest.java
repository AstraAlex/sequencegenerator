package ru.anovikov.interview.sequence;

import org.junit.Before;
import org.junit.Test;
import ru.anovikov.interview.sequence.core.impl.NumberSequence;
import ru.anovikov.interview.sequence.persistence.DBConnector;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class NumberSequenceTest {

    private final String TEST_SEQUENCE_NAME = "test_number_seq";
    private final int threadsCount = 3;
    private final int iterationsForThread = 3;

    @Before
    public void setUp() throws Exception {
        String dropTestSeq = "DROP SEQUENCE IF EXISTS " + TEST_SEQUENCE_NAME;

        try {
            Statement st = DBConnector.INSTANCE.getConnection().createStatement();
            st.execute(dropTestSeq);
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Test
    public void oneUserOneThread() {
        NumberSequence seq = new NumberSequence(TEST_SEQUENCE_NAME, 2, 5, true);
        seq.load();

        for (int i = 1; i <= 10; i++) {
            System.out.println(seq.currentNum());
            seq.nextValue();
        }
    }

    @Test
    public void oneUserMultiThreadTest() throws Exception {
        oneUserMultiThread();
    }

    @Test
    public void manyUsersMultiThread() {
        int userCount = 3;
        ThreadPoolExecutor usersPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(userCount);

        for (int i = 1; i <= userCount ; i++) {
            usersPool.submit(this::oneUserMultiThread);
        }

        waitForAllTaskComplete(usersPool);
    }

    private void oneUserMultiThread() {
        NumberSequence seq = new NumberSequence(TEST_SEQUENCE_NAME, null, null, false);
        seq.load();

        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadsCount);

        List<Integer> generatedNumbers = new ArrayList<>();

        for (int i = 1; i <= threadsCount; i++) {
            threadPool.submit(() -> {
                for (int j = 1; j <= iterationsForThread; j++) {

                    System.out.println(seq.currentNum() + " " + Thread.currentThread().getName());
                    generatedNumbers.add(seq.currentNum());
                    seq.nextValue();

                    work();
                }
            });
        }

        waitForAllTaskComplete(threadPool);

        Collections.sort(generatedNumbers);
        System.out.println("Обработаны номера " + generatedNumbers);
    }

    private void work() {
        try {
            TimeUnit.MILLISECONDS.sleep(ThreadLocalRandom.current().nextInt(100, 1000 + 1));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void waitForAllTaskComplete(ThreadPoolExecutor pool) {
        do {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (pool.getActiveCount() > 0);
    }
}