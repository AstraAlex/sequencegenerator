package ru.anovikov.interview.sequence;

import org.junit.Before;
import org.junit.Test;
import ru.anovikov.interview.sequence.core.impl.StringSequence;
import ru.anovikov.interview.sequence.persistence.DBConnector;

import java.sql.SQLException;
import java.sql.Statement;

public class StringSequenceTest {

    private final String TEST_SEQUENCE_NAME = "test_str_seq";

    @Before
    public void setUp() throws Exception {
        String dropTestSeq = "DROP SEQUENCE IF EXISTS " + TEST_SEQUENCE_NAME;

        try {
            Statement st = DBConnector.INSTANCE.getConnection().createStatement();
            st.execute(dropTestSeq);
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Test
    public void oneUserOneThreadWithLimit() throws Exception {
        StringSequence seq = new StringSequence(TEST_SEQUENCE_NAME, 3, 7, false, "ABC2017~~~");
        seq.load();

        for (int i = 1; i <= 10; i++) {
            System.out.println(seq.currentNum());
            seq.nextValue();
        }
    }
}
